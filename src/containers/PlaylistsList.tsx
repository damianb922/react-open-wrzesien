import {
  connect,
  MapStateToPropsParam,
  MapDispatchToPropsNonObject,
  MapDispatchToPropsParam
} from "react-redux";
import { ItemsList } from "../components/playlists/ItemsList";
import { AppState } from "../store";
import { Playlist } from "../models/Playlist";
import {
  selectPlaylists,
  selectSelectedPlaylist,
  playlistSelect
} from "../reducers/playlists.reducer";
import { bindActionCreators } from "redux";

export const PlaylistsList = connect(
  (state: AppState) => ({
    playlists: selectPlaylists(state).items,
    selected: selectSelectedPlaylist(state)
  }),

  dispatch => bindActionCreators({
    onSelect: p => playlistSelect(p.id),
  }, dispatch)

)(ItemsList);
