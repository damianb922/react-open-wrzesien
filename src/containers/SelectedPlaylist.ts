import { connect } from "react-redux";
import { PlaylistDetails } from "../components/playlists/PlaylistDetails";
import { AppState } from "../store";
import {
  selectSelectedPlaylist,
  playlistUpdate
} from "../reducers/playlists.reducer";
import { PlaylistForm } from "../components/playlists/PlaylistForm";
import { Playlist } from "../models/Playlist";
import { bindActionCreators } from "redux";

export const connectPlaylist = connect(
  (state: AppState) => ({
    playlist: selectSelectedPlaylist(state)!
  }),

  dispatch => bindActionCreators({ 
    onSave: playlistUpdate
  }, dispatch)
);

export const SelectedPlaylist = connectPlaylist(PlaylistDetails);
export const SelectedPlaylistForm = connectPlaylist(PlaylistForm);

// https:// bit.ly / 2k iV Oj0

// https:// bitbucket.org/ev45ive/react-open-wrzesien/

// dispatch => ({
//   onSave(draft: Playlist) {
//     dispatch(playlistUpdate(draft));
//   }
// })
