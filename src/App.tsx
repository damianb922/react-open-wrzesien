import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { MusicSearchView } from "./views/MusicSearchView";
import { MusicProvider } from "./services/MusicProvider";
import PlaylistsView from "./views/PlaylistsView";
import { Route, Switch, NavLink } from "react-router-dom";
// import './App.scss'

const Layout: React.FC<{}> = props => {
  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <a className="navbar-brand" href="#">
          Navbar
        </a>

        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink className="nav-link" to="/playlists">
                Playlists
              </NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" to="/search">
                Search
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
      <div className="container">
        <div className="row">
          <div className="col">
            <h3>Hello React</h3>
            {props.children}
          </div>
        </div>
      </div>
    </>
  );
};
const App: React.FC = () => {
  return (
    <div className="App">
      <MusicProvider>
        <Layout>
          <Switch>
            <Route path="/" exact={true} component={PlaylistsView} />
            <Route path="/search/:query" component={MusicSearchView} />
            <Route path="/search" component={MusicSearchView} />
            <Route path="/playlists" component={PlaylistsView} />
          </Switch>
        </Layout>
      </MusicProvider>
    </div>
  );
};

export default App;
