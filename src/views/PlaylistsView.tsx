import React from "react";
import { ItemsList } from "../components/playlists/ItemsList";
import { PlaylistDetails } from "../components/playlists/PlaylistDetails";
import { PlaylistForm } from "../components/playlists/PlaylistForm";
import { Playlist } from "../models/Playlist";
import { PlaylistsList } from "../containers/PlaylistsList";
import {
  SelectedPlaylist,
  SelectedPlaylistForm
} from "../containers/SelectedPlaylist";

enum Modes {
  show = "show",
  edit = "edit"
}

type S = {
  mode: Modes;
};

export class PlaylistsView extends React.Component<{}, S> {
  state: S = {
    mode: Modes.show // "show"
  };
  showForm = () => this.setState({ mode: Modes.edit });

  closeForm = () => this.setState({ mode: Modes.show });

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <PlaylistsList />
          </div>
          <div className="col">
            {this.state.mode === Modes.show && (
              <SelectedPlaylist onEdit={this.showForm} />
            )}

            {this.state.mode === Modes.edit && (
              <SelectedPlaylistForm onCancel={this.closeForm} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
