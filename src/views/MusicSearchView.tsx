import React from "react";
import { SearchForm } from "../components/search/SearchForm";
import { SearchResults } from "../components/search/SearchResults";
import { musicSearchContext } from "../services";
import { RouterProps, RouteComponentProps } from "react-router";

type P = {} & RouteComponentProps<{ query: string }>;

export class MusicSearchView extends React.Component<P> {
  static contextType = musicSearchContext;

  componentDidMount() {
    if (this.props.match.params.query)
      this.context.search(this.props.match.params.query);
  }

  componentDidUpdate() {
    if (this.props.match.params.query !== this.context.query) {
      this.props.history.push("/search/" + this.context.query);
    }
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <musicSearchContext.Consumer>
              {({ search }) => <SearchForm onSearch={search} />}
            </musicSearchContext.Consumer>
          </div>
        </div>

        <div className="row">
          <div className="col">
            <musicSearchContext.Consumer>
              {({ results }) => <SearchResults results={results} />}
            </musicSearchContext.Consumer>
          </div>
        </div>
      </div>
    );
  }
}
