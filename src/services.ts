import { AuthService } from "./services/Auth";
import { MusicSearch } from "./services/MusicSearch";
import axios from "axios";
import React from "react";
import { Album } from "./models/Album";

export const auth = new AuthService(
  "https://accounts.spotify.com/authorize",
  "a4cb43ea0e1c49178759f1c528abdc7e",
  "http://localhost:3000/"
);

axios.interceptors.request.use(
  config => {
    config.headers["Authorization"] = `Bearer ${auth.getToken()}`;
    return config;
  },
  err => err
);
axios.interceptors.response.use(
  r => r,
  err => {
    if (err.response.status === 401) {
      auth.authorize();
    }
    return err;
  }
);

export const musicSearch = new MusicSearch(
  "https://api.spotify.com/v1/search",
  auth
);

type S = {
  results: Album[];
  query: string;
  message: string;
  isLoading: boolean;
  search(q: string): any;
};

export const musicSearchContext = React.createContext<S>({
  results: [],
  query: "",
  message: "",
  isLoading: false,
  search(q: string) {
    throw Error("No provider for music search ");
  }
});
