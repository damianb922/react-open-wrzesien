import React from "react";
import { AlbumCard } from "./AlbumCard";
import styles from "./SearchResults.module.css";
import { Album } from "../../models/Album";

type P = {
  results: Album[];
  message?: string;
};

export const SearchResults: React.FC<P> = ({ results, message }) => (
  <div>
    {message && <p className="text-danger">{message}</p>}
    <div className="card-group">
      {results.map(result => (
        <AlbumCard album={result} key={result.id} className={styles.card} />
      ))}
    </div>
  </div>
);
