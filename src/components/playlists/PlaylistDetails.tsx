import React from "react";
import { Playlist } from "../../models/Playlist";

type P = {
  playlist: Playlist;
  onEdit(): void;
};

export const PlaylistDetails: React.FC<P> = ({ playlist, onEdit }) => (
  <div>
    <dl title={playlist.name} data-placki={123 + 5}>
      <dt>Name:</dt>
      <dd>{playlist.name}</dd>

      <dt>Favorite:</dt>
      <dd>{playlist.favorite ? "Yes" : "No"}</dd>

      <dt>Color:</dt>
      <dd
        style={{
          color: playlist.color,
          backgroundColor: playlist.color
        }}
      >
        {playlist.color}
      </dd>
    </dl>

    <input
      type="button"
      className="btn btn-info"
      value="Edit"
      onClick={onEdit}
    />
  </div>
);
