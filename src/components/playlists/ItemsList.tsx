import React from "react";
import { Playlist } from "../../models/Playlist";

type P = {
  playlists: Playlist[];
  selected: Playlist | null;
  onSelect(selected: Playlist): void;
  title?:string
};

export class ItemsList extends React.Component<P> {

  static initialProps = {
    title:'Placki',
    selected: null
  }

  select(p: Playlist) {
    this.props.onSelect(p);
  }

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((p, index, all) => (
            <div
              className={`list-group-item ${
                p === this.props.selected ? " active" : ""
              }`}
              onClick={() => this.select(p)}
              key={p.id}
            >
              {index + 1}. <span>{p.name}</span>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
