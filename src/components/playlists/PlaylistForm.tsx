import React from "react";
import { Playlist } from "../../models/Playlist";

type S = {
  playlist: Playlist;
};

type P = {
  playlist: Playlist;
  onCancel(): void;
  onSave(draft: Playlist): void;
};

export class PlaylistForm extends React.PureComponent<P, S> {
  constructor(props: P) {
    super(props);
    console.log("constructor");
    this.state = { playlist: props.playlist };
  }

  componentDidMount() {
    console.log("componentDidMount");
    if (this.placki_inputRef.current) {
      this.placki_inputRef.current.focus();
    }
  }

  static getDerivedStateFromProps(newProps: P, nextState: S) {
    console.log("getDerivedStateFromProps");

    return {
      playlist:
        nextState.playlist.id !== newProps.playlist.id
          ? newProps.playlist
          : nextState.playlist
    };
  }

  // shouldComponentUpdate(nextProps: Readonly<P>, nextState: Readonly<S>) {
  //   console.log("shouldComponentUpdate");
  //   return (
  //     this.props.playlist !== nextProps.playlist ||
  //     this.state.playlist !== nextState.playlist
  //   );
  // }

  getSnapshotBeforeUpdate(prevProps: Readonly<P>, prevState: Readonly<S>) {
    console.log("getSnapshotBeforeUpdate");
    return { ala: "placki" };
  }

  placki_inputRef = React.createRef<HTMLInputElement>();

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot?: any
  ) {
    console.log("componentDidUpdate", snapshot);
  }

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  save = () => {
    this.props.onSave(this.state.playlist);
  };

  handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const fieldName = target.name;

    // this.state.playlist['name'] = value as string

    this.setState({
      playlist: {
        ...this.state.playlist,
        [fieldName]: value
      }
    });
  };

  render() {
    console.log("render");
    return (
      <div>
        <div className="form-group">
          <label>Name:</label>
          <input
            ref={this.placki_inputRef}
            type="text"
            className="form-control"
            value={this.state.playlist.name}
            name="name"
            onChange={this.handleInput}
          />
          {/* {this.placki_inputRef.current!.value} */}
          {255 - this.state.playlist.name.length} / 255
        </div>

        <div className="form-group">
          <label>Favorite:</label>
          <input
            type="checkbox"
            checked={this.state.playlist.favorite}
            name="favorite"
            onChange={this.handleInput}
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            value={this.state.playlist.color}
            name="color"
            onChange={this.handleInput}
          />
        </div>

        <input
          type="button"
          className="btn btn-danger"
          value="Cancel"
          onClick={this.props.onCancel}
        />
        <input
          type="button"
          className="btn btn-success"
          value="save"
          onClick={this.save}
        />
      </div>
    );
  }
}
