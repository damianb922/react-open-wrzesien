function echo(text){
	return new Promise((resolve)=>{

        setTimeout(()=>{
           resolve( text )
        },1000)
    })

}

p1 = echo('placki')

p2 = p1.then(r => {
	return echo(r + ' malinowe ')
})

p2.then(r2 => console.log(r2) )


p3 = p2.then(r => {
	return echo(r + ' z czekolada ')
})

p3.then(r2 => console.log(r2) )


