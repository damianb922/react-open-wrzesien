import { Action, ActionCreator, Reducer } from "redux";

type AllActions = INCREMENT | DECREMENT;

export const counterReducer: Reducer<number, AllActions> = (
  state = 0,
  action
) => {
  switch (action.type) {
    case "INCREMENT":
      return state + action.payload;

    case "DECREMENT":
      return state - action.payload;

    default:
      return state;
  }
};

export interface INCREMENT extends Action<"INCREMENT"> {
  payload: number;
}

export interface DECREMENT extends Action<"DECREMENT"> {
  payload: number;
}

export const inc: ActionCreator<INCREMENT> = (payload = 1) => ({
  type: "INCREMENT",
  payload
});

export const dec: ActionCreator<DECREMENT> = (payload = 1) => ({
  type: "DECREMENT",
  payload
});
