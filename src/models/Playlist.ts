export interface Playlist {
  id: string | number;
  name: string;
  favorite: boolean;
  /**
   * Hex value for color
   */
  color: string;
  // tracks: Array<Track>
  tracks?: Track[];
}

export interface Track {
  id: number;
  name: string;
}


// const x:Playlist={}
