import React from "react";
import { musicSearch, musicSearchContext } from "../services";
import { Album } from "../models/Album";

type S = {
  results: Album[];
  message: string;
  query: string;
  isLoading: boolean;
};

export class MusicProvider extends React.Component<{}, S> {
  state = {
    results: [
      {
        id: "123",
        name: "test 123",
        images: [{ url: " https://www.placecage.com/gif/300/300" }]
      },
      {
        id: "234",
        name: "test 234",
        images: [{ url: " https://www.placecage.com/gif/200/200" }]
      },
      {
        id: "345",
        name: "test 345",
        images: [{ url: " https://www.placecage.com/gif/400/400" }]
      },
      {
        id: "456",
        name: "test 456",
        images: [{ url: " https://www.placecage.com/gif/500/500" }]
      }
    ],
    query: "",
    message: "",
    isLoading: false
  };

  search = (query: string) => {
    this.setState({ query, results: [], message: "", isLoading: true });
    musicSearch
      .search(query)
      .then(albums => this.setState({ results: albums }))
      .catch(error => this.setState({ message: error }))
      .finally(() => this.setState({ isLoading: false }));
  };

  render() {
    return (
      <musicSearchContext.Provider
        value={{
          ...this.state,
          search: this.search
        }}
      >
        {this.props.children}
      </musicSearchContext.Provider>
    );
  }
}
